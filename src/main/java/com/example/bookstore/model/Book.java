package com.example.bookstore.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "book")
@Entity
@Getter
@Setter
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Lob
    @Column(name = "author", nullable = false)
    private String author;

    @Lob
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "page_count", nullable = false)
    private Integer pageCount;
}