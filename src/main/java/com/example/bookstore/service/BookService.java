package com.example.bookstore.service;

import com.example.bookstore.model.Book;
import com.example.bookstore.repository.BookRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BookService {

    private final BookRepository bookRepository;

    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public Book create(Book book){
        return bookRepository.save(book);
    }

    public Optional<Book> read(Long id){
        return bookRepository.findById(id);
    }

    public Optional<Book> update(Long id, Book book){
        boolean exists = bookRepository.existsById(id);
        return exists ? Optional.of(bookRepository.save(book)) : Optional.empty();
    }

    public void delete(Long id){
        bookRepository.deleteById(id);
    }

}
